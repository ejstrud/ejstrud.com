---
title: "About"
date: 2020-07-14T18:29:34+02:00
draft: false
---

Hi, I'm Johan.

You can find me on [LinkedIn](https://www.linkedin.com/in/johanejstrud/), [GitHub](https://github.com/johan-ejstrud), or [write me an email](mailto:johan@ejstrud.com).

#### How It's Made
This website is made with [Hugo](https://gohugo.io/). I'm using [my own fork](https://github.com/johan-ejstrud/devise) of the theme [Devise](https://github.com/austingebauer/devise) created by [Austin Bauer](https://austingebauer.com/).

You can view the [source on GitHub](https://github.com/johan-ejstrud/ejstrud.com).
